DUMMY_OCCUPATION = {
   "occupation": {
        "onet": "15-1131",
        "title": "Computer Programmers"
   },
   "region": {
        "type": "MSA",
        "id": "42660",
        "title": "Seattle-Tacoma-Bellevue, WA"
   },
   "summary": {
        "jobs": {
             "year": 2015,
             "regional": 12352,
             "national_avg": 6501
        },
        "jobs_growth": {
             "start_year": 2013,
             "end_year": 2018,
             "regional": 10.2,
             "national_avg": 8.5
        },
        "earnings": {
             "regional": 57.24,
             "national_avg": 38.2
        }
   },
   "trend_comparison": {
        "start_year": 2013,
        "end_year": 2018,
        "regional": [
          11500,
          11580,
          11620,
          11630,
          11550,
          11650,
          11715,
          11725,
          11750,
          11775,
          11750,
          11800,
          11904,
          12384,
          12352,
          12680,
          12920,
          13114,
          13200,
          13250,
          13270,
          13290,
          13310,
          13330,
          13350
        ],
        "state": [
          12835,
          12830,
          12850,
          12860,
          12865,
          12875,
          12870,
          12890,
          12920,
          12975,
          13100,
          13120,
          13103,
          13598,
          13599,
          13968,
          14244,
          14469,
          14300,
          14500,
          14500,
          14530,
          14580,
          14670,
          14730
        ],
        "nation": [
          300000,
          300000,
          300000,
          300000,
          300000,
          300000,
          300000,
          300000,
          300000,
          300000,
          300000,
          300000,
          300651,
          307024,
          314154,
          318998,
          326205,
          320000,
          320000,
          320000,
          320000,
          320000,
          320000,
          320000,
          320000
        ]
   },
   "employing_industries": {
        "year": 2015,
        "jobs": 12352,
        "industries": [
             {
                  "naics": "511210",
                  "title": "Software Publishers",
                  "in_occupation_jobs": 4654,
                  "jobs": 52886
             },
             {
                  "naics": "541512",
                  "title": "Computer Systems Design Services",
                  "in_occupation_jobs": 1873,
                  "jobs": 20582
             },
             {
                  "naics": "541512",
                  "title": "Custom Computer Programming Services",
                  "in_occupation_jobs": 1388,
                  "jobs": 15252
             },
             {
                  "naics": "541512",
                  "title": "Aircraft Manufacturing",
                  "in_occupation_jobs": 444,
                  "jobs": 71612
             },
             {
                  "naics": "541512",
                  "title": "Other Computer Related Services",
                  "in_occupation_jobs": 296,
                  "jobs": 3245
             }
        ]
   }
}

class OccupationsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def show
    occupation = params.require(:occupation)
    area_type = params.require(:area_type)
    area_code = params.require(:area_code)
    respond_to do |format|
      format.html { render json: DUMMY_OCCUPATION }
      format.json { render json: DUMMY_OCCUPATION }
    end
  end
end